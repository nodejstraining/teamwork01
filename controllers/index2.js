var express  = require("express");
var router   = express.Router();
var models   = require("../models/schema");
var mongoose = require("mongoose");
var request  = require("request");

var mongo = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectID = require('mongodb').ObjectID;

var url =  'mongodb://localhost:27017/teamwork01';
router.route("/")
.get(function(req, res){
	res.render("index");
})
router.route("/home")
.get(function(req, res){
	var options = {
		
		url: "http://localhost:5000/api/user",
		method: "get"
	};

	request(options, function(err, response, user_data){
		if(!err && res.statusCode == 200){
			res.render('display',{
				user: JSON.parse(user_data).data
			})	
		}
	});
})
router.post('/delete',function(req,res){
	var id = req.body.id;
	mongo.connect(url,function(err,db){
		assert.equal(null,err);
		db.collection('users').deleteOne({"_id":ObjectID(id)},function(error,result){
			assert.equal(null,err);
			res.redirect('/home')
			db.close();
		});
	});
});
module.exports = router;